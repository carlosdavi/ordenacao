import matplotlib.pyplot as plt
import random
import timeit

def crialistarand(tam):
    lista = []
    random.seed()
    i = 0
    while i < tam:
        num = random.randint(1, 10 * tam)
        if num not in lista:
            lista.append(num)
        i += 1
    return lista

def bubbleSort(A):
    if len(A) <= 1:
        sA = A
    else:
        for j in range(0, len(A)):
            for i in range(0, len(A) - 1):
                if A[i] > A[i + 1]:
                    Aux = A[i + 1]
                    A[i + 1] = A[i]
                    A[i] = Aux
        sA = A

    return sA

def selectionSort(listaEntrada):
    for i in range(0, len(listaEntrada)):
        menor = i
        for k in range(i, len(listaEntrada)):
            if listaEntrada[k] < listaEntrada[menor]:
                menor = k
        #listaEntrada[menor], listaEntrada[i] = listaEntrada[i], listaEntrada[menor]
        aux = listaEntrada[i]
        listaEntrada[i] = listaEntrada[menor]
        listaEntrada[menor] = aux
    return listaEntrada

def insertionSort(l):
    for index in range(1, len(l)):

        currentvalue = l[index]
        position = index

        while position > 0 and l[position - 1] > currentvalue:
            l[position] = l[position - 1]
            position = position - 1

        l[position] = currentvalue

def quickSort(alist):
   quickSortHelper(alist,0,len(alist)-1)

def quickSortHelper(alist,first,last):
   if first<last:

       splitpoint = partition(alist,first,last)

       quickSortHelper(alist,first,splitpoint-1)
       quickSortHelper(alist,splitpoint+1,last)


def partition(alist,first,last):
   pivotvalue = alist[first]

   leftmark = first+1
   rightmark = last

   done = False
   while not done:

       while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
           leftmark = leftmark + 1

       while alist[rightmark] >= pivotvalue and rightmark >= leftmark:
           rightmark = rightmark -1

       if rightmark < leftmark:
           done = True
       else:
           temp = alist[leftmark]
           alist[leftmark] = alist[rightmark]
           alist[rightmark] = temp

   temp = alist[first]
   alist[first] = alist[rightmark]
   alist[rightmark] = temp


   return rightmark

lisb = [9,3,5,2,0]
list = crialistarand(1000);
print(lisb)
quickSort(lisb)
print(lisb)