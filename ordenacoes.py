# Aluno: Carlos David Lira
import timeit
import random

import math
import matplotlib.pyplot as plt

lTempoA = []
lTempoB = []
lTempoC = []
lTempoD = []
lTempoE = []
lTempoF = []
lTempoG = []
lTempoH = []
lTempoI = []
lTempoJ = []
lA = []

quanTam = []

tamTeste = 1000


def crialistarand(tam):
    random.seed()
    i = 0
    while i < tam:
        num = random.randint(1, 10 * tam)
        if num not in lA:
            lA.append(num)
        i += 1
    return lA


def selection(l):
    for i in range(0, len(l)):
        menor = i
        for k in range(i, len(l)):
            if l[k] < l[menor]:
                menor = k
        l[menor], l[i] = l[i], l[menor]
    return l


def bubble(A):
    if len(A) <= 1:
        sA = A
    else:
        for j in range(0, len(A)):
            for i in range(0, len(A) - 1):
                if A[i] > A[i + 1]:
                    Aux = A[i + 1]
                    A[i + 1] = A[i]
                    A[i] = Aux
        sA = A

    return sA


def insertionSort(l):
    for index in range(1, len(l)):

        currentvalue = l[index]
        position = index

        while position > 0 and l[position - 1] > currentvalue:
            l[position] = l[position - 1]
            position = position - 1

        l[position] = currentvalue


def quickSort(lista):
    L = []
    R = []
    if len(lista) <= 1:
        return lista
    chave = lista[len(lista) // 2]
    for i in lista:
        if i < chave:
            L.append(i)
        if i > chave:
            R.append(i)
    return quickSort(L) + [chave] + quickSort(R)


def mergeSort(lista):
    if len(lista) < 2:
        return lista
    metade = int(len(lista) / 2)
    return merge(mergeSort(lista[:metade]), mergeSort(lista[metade:]))


def merge(elista, dlista):
    final = []
    while elista and dlista:

        if len(elista) and len(dlista):
            if elista[0] < dlista[0]:
                final.append(elista.pop(0))
            else:
                final.append(dlista.pop(0))

        if not len(elista) and len(dlista):
            final.append(dlista.pop(0))
        if not len(dlista) and len(elista):
            final.append(elista.pop(0))

    return final


def shellSort(lista):
    gap = int(len(lista) / 2)
    while (gap > 0):
        for i in range(gap, len(lista)):
            menor_valor = lista[i]
            j = i
            while j >= gap and lista[j - gap] > menor_valor:
                lista[j] = lista[j - gap]
                j -= gap
            lista[j] = menor_valor
        gap = int(gap / 2)
    return lista


def countingSort(lista):
    valorMax = max(lista)
    posicoesVetorContagem = valorMax + 1
    vetorContagem = [0] * posicoesVetorContagem
    for i in lista:
        vetorContagem[i] += 1
    i = 0
    for a in range(0, posicoesVetorContagem):
        for b in range(0, vetorContagem[a]):
            lista[i] = a
            i += 1
    return lista


def radixSort(lista):
    RADIX = 10
    comprimento_Maximo = False
    tmp, placement = -1, 1

    while not comprimento_Maximo:
        comprimento_Maximo = True
        baldes = [list() for _ in range(RADIX)]

        for i in lista:
            tmp = i // placement
            baldes[tmp % RADIX].append(i)
            if comprimento_Maximo and tmp > 0:
                comprimento_Maximo = False

        a = 0
    return lista


def hashing(lista):
    m = lista[0]
    for i in range(1, len(lista)):
        if (m < lista[i]):
            m = lista[i]
    result = [m, int(math.sqrt(len(lista)))]
    return result


def re_hashing(i, code):
    return int(i / code[0] * (code[1] - 1))


def bucketSort(lista):
    code = hashing(lista)
    buckets = [list() for _ in range(code[1])]
    for i in lista:
        x = re_hashing(i, code)
        buck = buckets[x]
        buck.append(i)
    for bucket in buckets:
        insertionSort(bucket)

    ndx = 0
    for b in range(len(buckets)):
        for v in buckets[b]:
            lista[ndx] = v
            ndx += 1


def heapSort(lista):
    tam = len(lista) - 1
    elementoPai = tam // 2
    for i in range(elementoPai, -1, -1):
        moveDown(lista, i, tam)
    for i in range(tam, 0, -1):
        if lista[0] > lista[i]:
            lista[0], lista[i] = lista[i], lista[0]
            moveDown(lista, 0, i - 1)
    return lista


def moveDown(lista, primeiro, ultimo):
    maior = 2 * primeiro + 1
    while (maior <= ultimo):

        if (maior < ultimo) and (lista[maior] < lista[maior + 1]):
            maior += 1

        if lista[maior] > lista[primeiro]:
            lista[primeiro], lista[maior] = lista[maior], lista[primeiro]
            primeiro = maior
            maior = 2 * primeiro + 1

        else:
            return

while tamTeste < 2401:
    quanTam.append(tamTeste)
    lA = (crialistarand(tamTeste))
    lTempoA.append(timeit.timeit("bubble({})".format(lA), setup="from __main__ import bubble", number=1))
    lTempoB.append(timeit.timeit("selection({})".format(lA), setup="from __main__ import selection", number=1))
    lTempoC.append(timeit.timeit("insertionSort({})".format(lA), setup="from __main__ import insertionSort", number=1))
    lTempoD.append(timeit.timeit("quickSort({})".format(lA), setup="from __main__ import quickSort", number=1))
    lTempoE.append(timeit.timeit("mergeSort({})".format(lA), setup="from __main__ import mergeSort", number=1))
    lTempoF.append(timeit.timeit("shellSort({})".format(lA), setup="from __main__ import shellSort", number=1))
    lTempoG.append(timeit.timeit("countingSort({})".format(lA), setup="from __main__ import countingSort", number=1))
    lTempoH.append(timeit.timeit("radixSort({})".format(lA), setup="from __main__ import radixSort", number=1))
    lTempoI.append(timeit.timeit("bucketSort({})".format(lA), setup="from __main__ import bucketSort", number=1))
    lTempoI.append(timeit.timeit("heapSort({})".format(lA), setup="from __main__ import heapSort", number=1))



    print(tamTeste)
    if not tamTeste > 100:
        tamTeste += 200
    else:
        tamTeste += 300


fig, ax = plt.subplots()
plt.plot(quanTam, lTempoA,label="BubleSort")
plt.plot(quanTam, lTempoB,label="SelectionSort")
plt.plot(quanTam, lTempoC,label="InsertionSort")
plt.plot(quanTam, lTempoD, label="QuickSort")
plt.plot(quanTam, lTempoE, label="MergeSorte")
plt.plot(quanTam, lTempoF, label="ShellSort")
plt.plot(quanTam, lTempoG, label="CountingSort")
plt.plot(quanTam, lTempoH, label="radixSort")
plt.plot(quanTam, lTempoI, label="BucketSort")
plt.plot(quanTam, lTempoJ, label="heapSort")



legend = ax.legend(loc='upper center', shadow=True)
frame = legend.get_frame()
frame.set_facecolor('0.90')

plt.xlabel('tamanhos')
plt.ylabel('tempos')
plt.show()